<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//resource rutas campeonato
Route::resource( 'campeonato', 'CampeonatoController' );
//resource rutas evento
Route::resource( 'evento', 'EventoController' );
//resource rutas medallero
Route::resource( 'medallero', 'MedalleroController' );
//resource rutas socio
Route::resource( 'socio', 'SocioController' );
//resource rutas video
Route::resource( 'video', 'VideoController' );
//Auth::routes(['register' => false]);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
