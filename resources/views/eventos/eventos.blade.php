@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <ul>
                    @foreach( $eventos as $evento )
                        <li>{{ $evento->nombreEvento }}</li>
                        <li>{{ $evento->descripcionEvento }}</li>
                        <li>{{ date( 'd-m-Y h:m:s a', strtotime( $evento->fechaHora ) ) }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
    <div class="container">
        <div role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs nav-justified" role="tablist">
                <li class="tabHover" role="presentation" ng-class="{'active': tab == 1}">
                    <a ng-click="tab = 1" role="tab" data-toggle="tab">Eventos</a>
                </li>
                <li class="tabHover" role="presentation" ng-class="{'active': tab == 2}">
                    <a ng-click="tab = 2" role="tab" data-toggle="tab">Videos</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div ng-show="tab == 1" role="tabpanel" class="tab-pane active">
                    <button class="btn btn-primary text-right"
                            data-toggle="modal"
                            data-target="#mdlAgregarEventos">
                        <i class="fa fa-plus" aria-hidden="true"></i> Agregar Nuevo Evento
                    </button>
                    <hr>
                    <h3>Listado de eventos</h3>
                    <table class="table" id="tblEventos">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nombre</th>
                            <th scope="col">Descripción</th>
                            <th scope="col">Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach( $eventos as $evento )
                                <tr>
                                    <th scope="row">{{ $evento->idEvento }}</th>
                                    <th scope="row">{{ $evento->nombreEvento }}</th>
                                    <td scope="row">{{ $evento->descripcionEvento }}</td>
                                    {{--<td>{{ date( 'd-m-Y h:m:s a', strtotime( $evento->fechaHora ) ) }}</td>--}}
                                    <td>
                                        <div class="btn-group" role="group">
                                            <button class="btn btn-success"
                                                    ng-click="setearIdEvento(ev.idEvento)"
                                                    data-toggle="modal"
                                                    data-target="#mdlAgregarDocumentos">
                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                <i class="fa fa-picture-o" aria-hidden="true"></i>
                                            </button>
                                            <button class="btn btn-warning"
                                                    ng-click="consultarEditarEventos(ev.idEvento)"
                                                    data-toggle="modal"
                                                    data-target="#mdlEditarEventos">
                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                            </button>
                                            <button class="btn btn-danger"
                                                    ng-click="finalizarEvento(ev.idEvento)">
                                                <i class="fa fa-window-close-o" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div ng-show="tab == 2" role="tabpanel" class="tab-pane active">
                    <button class="btn btn-primary text-right"
                            data-toggle="modal"
                            data-target="#mdlAgregarVideo">
                        <i class="fa fa-plus" aria-hidden="true"></i> Agregar Nuevo Video
                    </button>
                    <hr>
                    <h3>Listado de videos</h3>
                    <table class="table table-striped" id="tblVideos">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Estado</th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <th>idVideo</th>
                            <td>estadoVideo</td>
                            <td>nombreVideo</td>
                            <td>descripcionVideo</td>
                            <td>
                                <button class="btn btn-warning btn-sm"
                                        ng-click="editarVideo(v.idVideo)"
                                        data-toggle="modal"
                                        data-target="#mdlEditarVideo">
                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Editar Datos Video
                                </button>
                                <button class="btn btn-danger btn-sm"
                                        ng-click="ocultarVideo(v.idVideo)">
                                    <i class="fa fa-window-close-o" aria-hidden="true"></i> OcultarVideo</button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>



        <!-- ModalNuevoEvento -->
        <div class="modal fade" id="mdlAgregarEventos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar datos de evento</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="nombreEvento">Nombre</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        id="nombreEvento"
                                        ng-model="nombreEvento"
                                        placeholder="Nombre del evento">
                            </div>
                            <div class="form-group">
                                <label for="descripcion">Descripción</label>
                                <textarea
                                        class="form-control"
                                        rows="3"
                                        id="descripcionEvento"
                                        ng-model="descripcionEvento"
                                        placeholder="Descripción"></textarea>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" ng-click="guardarEvento( nombreEvento, descripcionEvento );">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /ModalNuevoEvento -->

        <!-- ModalNuevoVideo -->
        <div class="modal fade" id="mdlAgregarVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar video nuevo</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="urlVideo">Url</label>
                                <input type="text"
                                       class="form-control"
                                       ng-model="videoUrl">
                            </div>
                            <div class="form-group">
                                <label for="nombreVideo">Nombre</label>
                                <input type="text"
                                       class="form-control"
                                       ng-model="videoNombre">
                            </div>
                            <div class="form-group">
                                <label for="descripcionVideo">Descripción</label>
                                <textarea class="form-control"
                                          rows="3"
                                          ng-model="videoDescripcion"></textarea>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-success" ng-click="guardarVideo();">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /ModalNuevoVideo -->

        <!-- modalEditarEvento -->
        <div class="modal fade" id="mdlEditarVideo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Editar datos del video</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="urlVideo">Url</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        ng-model="updateUrl">
                            </div>
                            <div class="form-group">
                                <label for="nombreVideo">Nombre</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        ng-model="updateNombre">
                            </div>
                            <div class="form-group">
                                <label for="descripcionVideo">Descripción</label>
                                <textarea
                                        class="form-control"
                                        rows="3"
                                        ng-model="updateDescripcion"></textarea>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-success"
                                ng-click="actualizarVideo( edUrlVideo, edNnombreVideo, edDescripcionVideo );">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">
                            <i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /modalEditarEvento -->

        <!-- modalEditarEvento -->
        <div class="modal fade" id="mdlEditarEventos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Editar datos del evento</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label for="edNnombreEvento">Nombre</label>
                                <input
                                        type="text"
                                        class="form-control"
                                        id="edNnombreEvento"
                                        ng-model="edNnombreEvento"
                                        placeholder="Nombre del evento">
                            </div>
                            <div class="form-group">
                                <label for="edDescripcionEvento">Descripción</label>
                                <textarea
                                        class="form-control"
                                        rows="3"
                                        id="edDescripcionEvento"
                                        ng-model="edDescripcionEvento"
                                        placeholder="Descripción"></textarea>
                            </div>

                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-success"
                                ng-click="guardarEventoEditado( edNnombreEvento, edDescripcionEvento, edIdEvento );">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        <button type="button"
                                class="btn btn-default"
                                data-dismiss="modal">
                            <i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /modalEditarEvento -->

        <!-- ModalAgregarDocumentos -->
        <div class="modal fade" id="mdlAgregarDocumentos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Agregar documentos a este evento</h4>
                    </div>
                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <input id="documentos" name="documentos[]" type="file" multiple>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            <i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /ModalAgregarDocumentos -->
    </div>
@endsection