<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Evento extends Model
{
    //nombre tabla
    protected $table = 'evento';
    //para formatear fechas
    protected $dates = ['created_at', 'updated_at', 'deleted_at'];
}
