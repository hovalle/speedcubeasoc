<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
        /*
         * Campeonato
         * Socio
         * Medallero
         * Evento
         * Video
         * */
    }
}

//C:\Windows\System32\cmd.exe
//C:\Program Files\Git\bin\sh.exe
