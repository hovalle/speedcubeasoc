<?php

namespace App\Http\Controllers;

use App\Medallero;
use Illuminate\Http\Request;

class MedalleroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medallero  $medallero
     * @return \Illuminate\Http\Response
     */
    public function show(Medallero $medallero)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Medallero  $medallero
     * @return \Illuminate\Http\Response
     */
    public function edit(Medallero $medallero)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medallero  $medallero
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medallero $medallero)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Medallero  $medallero
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medallero $medallero)
    {
        //
    }
}
